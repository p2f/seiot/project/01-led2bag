/*
 * IoT 2019/2020
 * Assignment #1: Led to Bag
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
*/

#include "Led2Bag.h"

#define SERIAL_BAUDRATE 9600
#define PIN_RND A1

void setup()
{
  Serial.begin(SERIAL_BAUDRATE);

  if (!initializeSeed(PIN_RND))
  {
    Serial.println("[X] Random seed not initialized: wrong pin used (use an analog pin).");
    while (true)
    {
    } /* Abort */
  }

  pinMode(PIN_L1, OUTPUT);
  pinMode(PIN_L2, OUTPUT);
  pinMode(PIN_L3, OUTPUT);
  pinMode(PIN_LB, OUTPUT);
  pinMode(PIN_LR, OUTPUT);

  resetStatus();
}

void loop()
{
  // Call the indexed FSM function
  states[currentStateIndex]();
}