/*
 * IoT 2019/2020
 * Assignment #1: Led to Bag
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
*/

#include "Led2Bag.h"

void (*states[STATE_COUNT])(void) = {&initL2B, &welcome, &setupGame, &setupRound, &gameLoop, &moveObject, &win, &gameOver};

int currentStateIndex = STATE_INDEX_INIT;

int leds[LED_COUNT] = {PIN_L1, PIN_L2, PIN_L3};
int objectPosition;
int objectsInBag = 0;

int difficulty = 1;
uint64_t maxRoundTime = DEAFULT_ROUND_TIME;
uint64_t elapsedTimeSinceNextButtonPress = millis();
uint64_t gameStartedTime;

void moveObject()
{
  if (millis() - elapsedTimeSinceNextButtonPress > DEBOUNCE_DELAY)
  {
    elapsedTimeSinceNextButtonPress = millis();

    digitalWrite(leds[objectPosition], LOW);
    objectPosition--;
    if (objectPosition >= 0 && objectPosition < LED_COUNT)
    {
      digitalWrite(leds[objectPosition], HIGH);
    }
    else
    {
      if (objectPosition == -1)
      {
        digitalWrite(PIN_LB, HIGH);
      }
      else
      {
        currentStateIndex = STATE_INDEX_GAMEOVER;
        return;
      }
    }
  }
  currentStateIndex = STATE_INDEX_WAITFORINPUT;
}

void win()
{
  objectsInBag++;
  Serial.println(MSG_BAG(objectsInBag));
  maxRoundTime -= (maxRoundTime * DIFFULTY_SCALE);
  pulse(PIN_LB, WIN_LED_DURATION, WIN_PULSE_STEP);
  currentStateIndex = STATE_INDEX_GAME;
}

void powerOffObjectsLeds()
{
  for (int i = 0; i < LED_COUNT; i++)
  {
    digitalWrite(leds[i], LOW);
  }
}

void printCurrentDifficulty()
{
  Serial.println(MSG_DIFFICULTY(difficulty));
}

void updateDifficulty()
{
  int potentiometerValue = (analogRead(PIN_PT) / POT_SCALE) + 1;
  if (potentiometerValue != difficulty)
  {
    difficulty = potentiometerValue;
    printCurrentDifficulty();
  }
}

void buttonObjectDown()
{
  currentStateIndex = STATE_INDEX_NEXTLED;
}

void checkBag()
{
  if (objectPosition == -1)
  {
    currentStateIndex = STATE_INDEX_WIN;
  }
  else
  {
    currentStateIndex = STATE_INDEX_GAMEOVER;
  }
}

void gameLoop()
{
  if (millis() - gameStartedTime > maxRoundTime)
  {
    checkBag();
  }
}

void setupRound()
{
  objectPosition = (int)random(LED_COUNT);

  digitalWrite(PIN_LB, LOW);
  digitalWrite(leds[objectPosition], HIGH);

  attachInterrupt(digitalPinToInterrupt(PIN_TD), buttonObjectDown, RISING);

  currentStateIndex = STATE_INDEX_WAITFORINPUT;

  elapsedTimeSinceNextButtonPress = millis();
  gameStartedTime = millis();
}

void startGameInt()
{
  currentStateIndex = STATE_INDEX_STARTGAME;
}

void setupGame()
{
  Serial.println(MSG_STARTGAME);

  detachInterrupt(digitalPinToInterrupt(PIN_TS));
  powerOffObjectsLeds();

  objectsInBag = 0;
  maxRoundTime = DEAFULT_ROUND_TIME / difficulty;
  currentStateIndex = STATE_INDEX_GAME;
}

void initL2B()
{
  Serial.println(MSG_INIT);
  updateDifficulty();
  attachInterrupt(digitalPinToInterrupt(PIN_TS), startGameInt, RISING);
  currentStateIndex = STATE_INDEX_WELCOME;
}

void welcome()
{
  welcomeLeds();
  updateDifficulty();
}

void gameOver()
{
  Serial.println((String)MSG_GAMEOVER(objectsInBag));

  digitalWrite(PIN_LB, LOW);

  resetStatus();

  blink(PIN_LR, GAMEOVER_DURATION);

  currentStateIndex = STATE_INDEX_INIT;
}

void powerOffAllLeds()
{
  powerOffObjectsLeds();
  digitalWrite(PIN_LB, LOW);
  digitalWrite(PIN_LR, LOW);
}

void resetStatus()
{
  powerOffObjectsLeds();

  detachInterrupt(digitalPinToInterrupt(PIN_TS));
  detachInterrupt(digitalPinToInterrupt(PIN_TD));
}

void welcomeLeds()
{
  // L3 -> L1
  for (int i = LED_COUNT - 1; i >= 0; i--)
  {
    blink(leds[i], WELCOME_LED_DURATION);
  }

  // L1 -> L3
  for (int i = 1; i < LED_COUNT - 1; i++)
  {
    blink(leds[i], WELCOME_LED_DURATION);
  }
}