/*
 * IoT 2019/2020
 * Assignment #1: Led to Bag
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
*/

#ifndef LED2BAG
#define LED2BAG

#include <Arduino.h>
#include "L2BUtils.h"

#define LED_COUNT 3

#define PIN_L1 4
#define PIN_L2 9
#define PIN_L3 8

#define PIN_LB 5
#define PIN_LR 6

#define PIN_TS 2
#define PIN_TD 3
#define PIN_PT A0

#define MSG_INIT "Welcome to Led to Bag. Press Key TS to Start."
#define MSG_BAG(X) "Another object in the bag! Count: " + (String)X + " objects."
#define MSG_GAMEOVER(X) "Game Over - Score: " + X
#define MSG_STARTGAME "Go!"
#define MSG_DIFFICULTY(X) "Difficulty set to: " + (String)X

#define WELCOME_LED_DURATION 100
#define GAMEOVER_DURATION 2000
#define WIN_LED_DURATION 2000
#define WIN_PULSE_STEP 5

#define POT_SCALE 128

#define STATE_COUNT 8
#define STATE_INDEX_INIT 0
#define STATE_INDEX_WELCOME 1
#define STATE_INDEX_STARTGAME 2
#define STATE_INDEX_GAME 3
#define STATE_INDEX_WAITFORINPUT 4
#define STATE_INDEX_NEXTLED 5
#define STATE_INDEX_WIN 6
#define STATE_INDEX_GAMEOVER 7

#define DEAFULT_ROUND_TIME 10000L

#define DIFFULTY_SCALE (1 / 8.0)

#define DEBOUNCE_DELAY 150

extern void (*states[STATE_COUNT])(void);
extern int currentStateIndex;

/**
 * Called when the TD button is pressed.
 */
void buttonObjectDown();

/**
 * If the object is in the bag, player wins.
 * Else the game is lost.
 */
void checkBag();

/**
 * Check the bag only when the timer expires.
 */
void gameLoop();

/**
 * Display error message, calls @see resetStatus(), blinks error LED.
 */
void gameOver();

/**
 * Led to Bag entry point: displays welcome message, current
 * difficulty and attaches interrupt for TS.
 */
void initL2B();

/**
 * Moves the object down to the bag.
 * If the object is moved outsite the bag, the player loses the game.
 */
void moveObject();

/**
 * Print game difficulty.
 */
void printCurrentDifficulty();

/**
 * Turns off all LEDs (object LEDs, bag and error LEDs).
 */
void powerOffAllLeds();

/**
 * Turns off objects LEDs (Bag LED and Error LED are not included).
 */
void powerOffObjectsLeds();

/**
 * Power off all LEDs and detaches buttons interrupts.
 */
void resetStatus();

/**
 * Setup game variables, detaches interrupt for TS, turn off all LEDs.
 */
void setupGame();

/**
 * Set the FSM state to initialize Game.
 * @see setupGame()
 */
void startGameInt();

/**
 * Setup round variables, set the random object position.
 */
void setupRound();

/**
 * Reads potentiometer value, sets the difficulty and prints it.
 * Can be executed only when the game is not started.
 */
void updateDifficulty();

/**
 * Wait for player to press TD button.
 */
void waitForInput();

/**
 * Registrates a round win:
 *  - increments objects in bag
 *  - prints a message
 *  - increments difficulty
 */
void win();

/**
 * Start blinking object LEDs and displays initial difficulty.
 * @see welcomeLeds()
 * @see updateDifficulty
 */
void welcome();

/**
 * Blink LEDs from top to bottom and reverse.
 */
void welcomeLeds();

#endif /* LED2BAG */