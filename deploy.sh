#!/bin/bash

FOLDER_NAME=Progetto-01
PROJECT_NAME=led_to_bag

mkdir ${FOLDER_NAME}
cp ./sketch_bb.png ${FOLDER_NAME}
cp ./sketch.fzz ${FOLDER_NAME}
mkdir -p ${FOLDER_NAME}/${PROJECT_NAME}
cp -R lib/ ${FOLDER_NAME}/${PROJECT_NAME}
cp -R src/ ${FOLDER_NAME}/${PROJECT_NAME}
zip -r ${FOLDER_NAME}.zip ${FOLDER_NAME}/
rm -rf ${FOLDER_NAME}
