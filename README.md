# Led 2 Bag

## Correzioni

### Sketch

- potenziometro colore segnale

### Codice

- insieme di led gestito come array (+ loro inizializzazione con for)

## Analysis diagram

### State diagram

```plantuml
hide empty description

state Game {
    [*] --> LedRandom
    LedRandom --> NextLed : Td
    NextLed ---> [*] : (Td && pos <= 0) ||\nDTExpired
    NextLed --> NextLed : Td &&\npos > 0
    LedRandom --> [*] : DTExpired
    NextLed --> Bag : pos == rand &&\nDTExpired
    Bag --> LedRandom
}

[*] -right-> Init
Init -down-> Game : Ts
Init -right-> GestionePot: RotazionePot
GestionePot --> Init
GameOver --> Init : wait(2s)
Game --right--> GameOver
```

## Project diagram

### State diagram

```plantuml
hide empty description

Init: entry/MsgWelcome
Init: shutdown leds
Init : L1-L3
Init: L3-L1
GestionePot: Modifica difficoltà
StartGame: entry/MsgStartGame
StartGame: shutdown leds
StartGame: bag = 0
GameOver: entry/MsgGameOver
GameOver: LedErr
GameOver: LB = 0

state Game {
    LedRandom: pos = rand()
    LedRandom: LB = 0
    NextLed: Lpos = 0
    NextLed: Lpos-1 = 1
    NextLed: pos--
    Bag: bag++
    Bag: LB = 1 (fade)

    [*] --> LedRandom
    LedRandom --> NextLed : Td
    NextLed ---> [*] : (Td && pos <= 0) ||\nDTExpired
    NextLed --> NextLed : Td &&\npos > 0
    LedRandom --> [*] : DTExpired
    NextLed --> Bag : pos == rand &&\nDTExpired
    Bag --> LedRandom
}

[*] -right-> Init
Init -down-> StartGame : Ts
StartGame -down-> Game
Init -right-> GestionePot: RotazionePot
GestionePot --> Init
GameOver --> Init : wait(2s)
Game --right-> GameOver
```

## Hardware schema

![](./sketch_bb.png)

## Simulator sketch

Available on [Tinkercad Circuits](https://www.tinkercad.com/things/cKaUiMTjajj-mighty-tumelo/editel?sharecode=PqGRzSOeoKKKs7c_qpbx57bXagF5x-TmhKehoIhomYU=).

## Pseudocode

```c
int difficoltàIniziale = 10;

/*
 * DATA:
 * LR -> variable
 * LB -> variable
 * game_leds -> array
*/

void resetStatus() {
    L* = 0;
    DISATTIVA_INT(*);
}

void init() {
    Serial(MSG_INIT);
    resetStatus();
    ATTIVA_INT(POT);
    ATTIVA_INT(Ts);
    Loop:
        L3 = 1; wait(500); L3 = 0;
        L2 = 1; wait(500); L2 = 0;
        L1 = 1; wait(500); L1 = 0;

        L1 = 1; wait(500); L1 = 0;
        L2 = 1; wait(500); L2 = 0;
        L3 = 1; wait(500); L3 = 0;
    goto Loop;
}

void gestionePotenziometro() { /* while Loop is executing */
    difficoltà = 1;
    /* triggered by potentiometer */
    difficoltà = valore_potenziometro / 128;
}

void gameOver() {
    Serial(MSG_GAMEOVER);
    if(pos == 0) {
        LB = 0;
    }
    LR = 1; wait(2000); LR = 0;
    resetStatus();
}

void game() { /* triggered at Ts press */
    DISATTIVA_INT(POT);
    DISATTIVA_INT(Ts);
    DT = difficoltàIniziale / difficoltà;
    bag = 0;
    seed(time());
    nextGame();
}

void nextGame() {
    pos = rand(1, 3);
    Lpos = 1;
    Tstart = time();
    ATTIVA_INT(Td);
}

void press() {
    if (pos > 0) {
        Lpos = 0;
        Lpos-1 = 1;
        pos--;
        if (pos == 0) {
            LB = 1;
        }
    }
    else {
        gameOver();
    }
}

void win() {
    bag++;
    Serial(MSG_WIN);
    DT /= 1/8;
    LB = 0;
}

void loop() {
    scadeDt();
}

void scadeDt() {
    DISATTIVA_INT(Td);
    if(time() - Tstart > DT) {
        if(pos == 0) {
            win();
            nextGame();
        }
        else {
            gameOver();
        }
    }
}
```
